package accounts

import (
	"database/sql"
	_ "gitee.com/wm1998/account/testx" // 【会先执行：此包的init()以构建自动化单元测试环境】
	"gitee.com/wm1998/infra/base"
	"github.com/segmentio/ksuid"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey" // 点操作的含义就是这个包导入之后在你调用这个包的函数时，你可以省略前缀的包名
	"github.com/tietang/dbx"
	"testing"
)

func TestAccountDao_GetOne(t *testing.T) {
	// 使用dbx提供的快捷的事务操作函数，在函数内完成的DB操作都认为是同一个事务，如果返回err则执行事务全部回滚
	err := base.Tx(func(runner *dbx.TxRunner) error { // 开启DB事务
		dao := &AccountDao{
			runner: runner,
		}
		Convey("通过编号查询账户数据", t, func() {
			a := &Account{ // 构建资金账户
				Balance:     decimal.NewFromFloat(100),
				Status:      1,
				AccountNo:   ksuid.New().Next().String(),
				AccountName: "测试资金账户",
				UserId:      ksuid.New().Next().String(),
				Username:    sql.NullString{String: "测试用户", Valid: true},
			}
			id, err := dao.Insert(a)       // 调用DAO层的插入函数，返回执行成功后生成的主键
			So(err, ShouldBeNil)           // 断言正确条件：err为nil
			So(id, ShouldBeGreaterThan, 0) // 断言正确条件：id大于0

			na := dao.GetOne(a.AccountNo)                            // 调用DAO层的查询函数
			So(na, ShouldNotBeNil)                                   // 断言正确条件：na不为nil
			So(na.Balance.String(), ShouldEqual, a.Balance.String()) // 断言正确条件：参数1=参数3，即：查询出的账户余额等于新插入账户的余额
			So(na.CreatedAt, ShouldNotBeNil)                         // 断言正确条件：查询出来的创建时间不为nil
			So(na.UpdatedAt, ShouldNotBeNil)
		})
		return nil
	})
	if err != nil {
		logrus.Error(err)
	}
}

func TestAccountDao_GetByUserId(t *testing.T) {
	err := base.Tx(func(runner *dbx.TxRunner) error {
		dao := &AccountDao{
			runner: runner,
		}
		Convey("通过用户ID和账户类型查询账户数据", t, func() {
			a := &Account{
				Balance:     decimal.NewFromFloat(100),
				Status:      1,
				AccountNo:   ksuid.New().Next().String(),
				AccountName: "测试资金账户",
				UserId:      ksuid.New().Next().String(),
				Username:    sql.NullString{String: "测试用户", Valid: true},
				AccountType: 2,
			}
			id, err := dao.Insert(a)
			So(err, ShouldBeNil)
			So(id, ShouldBeGreaterThan, 0)

			na := dao.GetByUserId(a.UserId, a.AccountType) // 调用DAO的执行查询函数
			So(na, ShouldNotBeNil)
			So(na.Balance.String(), ShouldEqual, a.Balance.String())
			So(na.CreatedAt, ShouldNotBeNil)
			So(na.UpdatedAt, ShouldNotBeNil)
		})
		return nil
	})
	if err != nil {
		logrus.Error(err)
	}
}

func TestAccountDao_UpdateBalance(t *testing.T) {
	// 使用dbx提供的快捷的事务操作函数来完成事务操作
	err := base.Tx(func(runner *dbx.TxRunner) error {
		dao := &AccountDao{
			runner: runner,
		}
		balance := decimal.NewFromFloat(100)

		Convey("更新账户余额", t, func() {
			a := &Account{
				Balance:     balance,
				Status:      1,
				AccountNo:   ksuid.New().Next().String(),
				AccountName: "测试资金账户",
				UserId:      ksuid.New().Next().String(),
				Username:    sql.NullString{String: "测试用户", Valid: true},
			}
			id, err := dao.Insert(a) // 调用DAO的插入函数
			So(err, ShouldBeNil)
			So(id, ShouldBeGreaterThan, 0)

			//1.增加余额
			Convey("增加余额", func() {
				amount := decimal.NewFromFloat(10)                  // 增加10元
				rows, err := dao.UpdateBalance(a.AccountNo, amount) // 调用DAO的更改函数
				So(err, ShouldBeNil)
				So(rows, ShouldEqual, 1)
				na := dao.GetOne(a.AccountNo)     // 调用DAO的查询函数
				newBalance := balance.Add(amount) // 涉及金额计算，要使用decimal包的函数
				So(na, ShouldNotBeNil)
				So(na.Balance.String(), ShouldEqual, newBalance.String())
			})

			//2.扣减余额，在余额足够的情况下
			//把updateBalance余额扣减的第二个测试用例
			//作为作业留给同学们来编写

			//3.扣减余额，在余额不够的情况下
			Convey("扣减余额，余额不够", func() {
				a1 := dao.GetOne(a.AccountNo)
				So(a1, ShouldNotBeNil)

				amount := decimal.NewFromFloat(-300)
				rows, err := dao.UpdateBalance(a.AccountNo, amount)
				So(err, ShouldBeNil)
				So(rows, ShouldEqual, 0)
				a2 := dao.GetOne(a.AccountNo)
				So(a2, ShouldNotBeNil)
				So(a1.Balance.String(), ShouldEqual, a2.Balance.String())
			})
		})
		return nil
	})
	if err != nil {
		logrus.Error(err)
	}
}
