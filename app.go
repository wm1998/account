package resk

import (
	_ "gitee.com/wm1998/account/apis/web"
	_ "gitee.com/wm1998/account/core/accounts"
	"gitee.com/wm1998/infra"
	"gitee.com/wm1998/infra/base"
)

// 注册资源服务启动器到注册启动器切片里
func init() {
	infra.Register(&base.PropsStarter{})       // 注册配置文件启动器
	infra.Register(&base.DbxDatabaseStarter{}) // 注册dbx数据库启动器
	infra.Register(&base.ValidatorStarter{})   // 注册验证器启动器

	infra.Register(&base.IrisServerStarter{}) // 注册Iris Web框架启动器
	infra.Register(&infra.WebApiStarter{})    // 注册Web框架启动器

	infra.Register(&base.EurekaStarter{}) // 注册Eureka客户端启动器

	infra.Register(&base.HookStarter{}) // 注册停止/退出进程启动器
}
