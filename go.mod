module gitee.com/wm1998/account

go 1.14

require (
	gitee.com/wm1998/infra v0.1.1
	github.com/kataras/iris v11.1.1+incompatible
	github.com/segmentio/ksuid v1.0.2
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/smartystreets/goconvey v0.0.0-20190306220146-200a235640ff
	github.com/tietang/dbx v1.0.1
	github.com/tietang/props v2.2.0+incompatible
)
