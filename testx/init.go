package testx

import (
	"gitee.com/wm1998/infra"
	"gitee.com/wm1998/infra/base"
	"github.com/tietang/props/ini"
	"github.com/tietang/props/kvs"
)

// 构建自动化单元测试环境
func init() {
	file := kvs.GetCurrentFilePath("../brun/config.ini", 1) //获取程序运行文件所在的路径
	conf := ini.NewIniFileCompositeConfigSource(file)       //加载和解析配置文件
	base.InitLog(conf)

	infra.Register(&base.PropsStarter{})
	infra.Register(&base.DbxDatabaseStarter{})
	infra.Register(&base.ValidatorStarter{})

	app := infra.New(conf)
	app.Start()
}
