#!/usr/bin/env bash

# 以下注释是实现：动态追加到GOPATH环境变量中
#cpath=`pwd`
#PROJECT_PATH=${cpath%src*}
#echo $PROJECT_PATH
#export GOPATH=$GOPATH:${PROJECT_PATH}

SOURCE_FILE_NAME=main
TARGET_FILE_NAME=reskd

rm -fr ${TARGET_FILE_NAME}*

build(){
  echo $GOOS $GOARCH
  tname=${TARGET_FILE_NAME}_${GOOS}_${GOARCH}${EXT}

  env GOOS=$GOOS GOARCH=$GOARCH
  go build -o ${tname} -v ${SOURCE_FILE_NAME}.go

  chmod +x ${tname}

  # 打包
  mv ${tname} ${TARGET_FILE_NAME}${EXT}
  # 如果操作系统是Windows
  if [ ${GOOS} == "windows" ];then
    zip ${tname}.zip ${TARGET_FILE_NAME}${EXT} config.ini ../public/
  else
    tar --exclude=*.gz --exclude=*.zip --exclude=*.git -zcvf ${tname}.tar.gz ${TARGET_FILE_NAME}${EXT} config.ini *
    .sh ../public/ -C ./ .
  fi
  mv ${TARGET_FILE_NAME}${EXT} ${tname}
}

# 因Golang项目交叉编译不支持CGO，所以要设置CGO_ENABLED变量为0，表示：禁用CGO
CGO_ENABLED=0

#mac os 64
GOOS=darwin
GOARCH=amd64
build

#linux 64
GOOS=linux
GOARCH=amd64
build

#windows
#64
GOOS=windows
GOARCH=amd64
build

GOARCH=386
build