package main

import (
	_ "gitee.com/wm1998/account" // 【注意】：需要使用_ "gitee.com/wm1998/account" 导入包来先执行app.go，从而先注册配置启动器到注册启动器切片里
	"gitee.com/wm1998/infra"     //【会执行：infra/banner.go的init()而加载banner】
	"gitee.com/wm1998/infra/base"
	"github.com/tietang/props/ini"
	"github.com/tietang/props/kvs"
)

// 系统启动
func main() {
	file := kvs.GetCurrentFilePath("config.ini", 1)   // 获取程序运行文件所在的路径
	conf := ini.NewIniFileCompositeConfigSource(file) // 加载和解析配置文件
	base.InitLog(conf)
	app := infra.New(conf) // 构建应用程序启动管理器
	app.Start()            // 初始化、安装、启动应用程序启动管理器
}
