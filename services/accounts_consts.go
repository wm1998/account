package services

const (
	//默认常数
	DefaultCurrencyCode = "CNY"
)

// 转账状态-枚举
type TransferedStatus int8

const (
	TransferedStatusFailure         TransferedStatus = -1 // 转账失败
	TransferedStatusSufficientFunds TransferedStatus = 0  // 余额不足
	TransferedStatusSuccess         TransferedStatus = 1  // 转账成功
)

// 转账的类型-枚举：0=创建账户 >=1进账 <=-1 支出
type ChangeType int8

const (
	AccountCreated       ChangeType = 0  // 账户创建
	AccountStoreValue    ChangeType = 1  // 储值/进账
	EnvelopeOutgoing     ChangeType = -2 // 红包资金的支出
	EnvelopeIncoming     ChangeType = 2  // 红包资金的收入
	EnvelopExpiredRefund ChangeType = 3  // 红包过期退款
)

// 资金交易变化标识-枚举
type ChangeFlag int8

const (
	FlagAccountCreated ChangeFlag = 0  // 创建账户
	FlagTransferOut    ChangeFlag = -1 // 支出
	FlagTransferIn     ChangeFlag = 1  // 收入
)

//账户类型
type AccountType int8

const (
	EnvelopeAccountType       AccountType = 1 // 红包账户类型
	SystemEnvelopeAccountType AccountType = 2 // 系统红包账户类型
)
